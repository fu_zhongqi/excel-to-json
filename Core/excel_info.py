class TitleFlag:
    # master='<'
    prime='*'
    Type='#'
    ignore='!'

class DataType:
    INT='int'
    FLOAT='float'
    STRING='string'
    BOOL='bool'
    LIST='[]'
    DICT='{}'
    UNKNOWN='unknown'

class TitleInfo:
    name=''
    isIgnore=False
    isPrimaryKey=False
    primaryKey='id'
    dataType=DataType.UNKNOWN